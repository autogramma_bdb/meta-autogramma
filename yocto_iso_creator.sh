#!/bin/bash

YOCTO_DEPLOY_LINK=$1
ISO_DEST_LINK=$2
ISO_EXTRA_INFO_LINK=$3

SPL_NAME=SPL-sd
UBOOT_NAME=u-boot.img-sd

SPL_NAME_TGT=SPL-nand
UBOOT_NAME_TGT=u-boot.img-nand

DTB_NAME=imx6q-ag-bdb.dtb
DTB_NAME_SOLO=imx6dl-ag-bdb.dtb
UIMAGE_NAME=uImage
ROOTFS_NAME=ag-image-gui-ag-bdb-mx6.ext4
ROOTFS_SIZE=( $(du -sLb $YOCTO_DEPLOY_LINK/$ROOTFS_NAME | grep -oE "\b[0-9]+") )

#Пути параметры временных файлов
TMPFOLDER=$ISO_DEST_LINK
TMPFILE=out.iso

#Параметры второго раздела
ROOTFS_ST_OFSET=`expr 24578 \* 512`
ROOTFS_FREE_SPACE=`expr 100 \* 1024 \* 1024`

die()
{
	STATUS=$?
	echo $1
	exit $STATUS
}

if [[ $UID != "0" ]]; then
	echo " "
	echo "*******************************************************************************"
	echo "Необходимы привелигии администратора для работы скрипта"
	echo "Перезапускаем себя от администратора"
	echo "*******************************************************************************"
	echo " "
	# Имя текущего скрипта
	SCRIPTNAME="$(realpath -s $0)"
	exec sudo $SCRIPTNAME $@
fi


echo " "
echo "*******************************************************************************"
echo "Этап 0: чистим временные файлы/папки"

echo " по пути $TMPFOLDER/$TMPFILE"
rm -rdf $TMPFOLDER || die

echo " "
echo "*******************************************************************************"
echo "Этап 1: Создаём файл заготовку"

TMPFILESIZE=`expr ${ROOTFS_ST_OFSET} + ${ROOTFS_SIZE} + ${ROOTFS_SIZE} + ${ROOTFS_FREE_SPACE} `

echo " по пути $TMPFOLDER/$TMPFILE размером $TMPFILESIZE Байт"
mkdir $TMPFOLDER -p || die
touch $TMPFOLDER/$TMPFILE || die
chmod -R 777 $TMPFOLDER || die
dd if=/dev/zero of=$TMPFOLDER/$TMPFILE bs=1K count=`expr ${TMPFILESIZE} / 1024 + 1` || die


echo " "
echo "*******************************************************************************"
echo "Этап 2: Делаём таблицу разделов"

echo " Монтируем образ как loop усройство"
node_1=$(losetup -f --show $TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]")

echo " Временное loop устройство: ${node_1}"
echo " "
(echo o; echo w ) | fdisk ${node_1}

echo " "
echo " Отключем устройстово ${node_1}"
sync
sleep 3
losetup -v -d ${node_1} || die
sync
sleep 3

echo " "
echo "*******************************************************************************"
echo "Этап 3: Размечаем разделы"

echo " Монтируем образ как loop усройство"
node_2=$(losetup -f --show $TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]")
sync
sleep 3

echo " Временное loop устройство: $node_2"
echo " "
sfdisk --force -uS ${node_2} << EOF
8193 16385 b
`expr ${ROOTFS_ST_OFSET} / 512` `expr $( expr ${TMPFILESIZE} - ${ROOTFS_ST_OFSET} ) / 512` 83;
EOF

echo " "
echo " Отключем устройстово $node_2"
sync
sleep 1
losetup -v -d $node_2


echo " "
echo "*******************************************************************************"
echo "Этап 4: Подключем разделы для установки образов (Boot/rootfs) LRT"

echo Монтируем образ как Loop усройство
node_3=$(losetup -f --show $TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]")

echo Временное loop устройство: $node_3
echo Подключаем разделы:
node_3_p=( $(kpartx -av ${node_3} | grep -oE "loop[0-9]p[0-9]") )
echo ${node_3_p[0]}, ${node_3_p[1]}
sync
sleep 1

echo " "
echo " Копируем через dd SPL"
dd if=$YOCTO_DEPLOY_LINK/$SPL_NAME of=${node_3} bs=512 seek=2
sync

echo " "
echo " Копируем через dd загрузчик $YOCTO_DEPLOY_LINK/$UBOOT_NAME"
dd if=$YOCTO_DEPLOY_LINK/$UBOOT_NAME of=${node_3} bs=512 seek=138
sync

echo " "
echo " Копируем через dd обр. root FS $YOCTO_DEPLOY_LINK/$ROOTFS_NAME"
dd if=$YOCTO_DEPLOY_LINK/$ROOTFS_NAME of=/dev/mapper/${node_3_p[1]}
sync
sleep 1

if [ -z "$ISO_EXTRA_INFO_LINK" ]
then
	echo " "
else
	echo " "
	echo "Копируем информацию о собраном софте в образ root FS"
	echo " "
	echo " подключаем раздел: ${node_3_p[1]}"
	mkdir $TMPFOLDER/img_fs -p; sync
	mount /dev/mapper/${node_3_p[1]} $TMPFOLDER/img_fs -t ext4
	
	mkdir -pv $TMPFOLDER/img_fs/etc/git_os_info
	cp -v $ISO_EXTRA_INFO_LINK/* $TMPFOLDER/img_fs/etc/git_os_info
	echo " "
	echo " Отключем устройстово  ${node_3_p[1]}"

	#2 раза потому что система маунтит ешё и по fstab и от них не отключает при одном umount
	sync
	sleep 3
	umount -dl /dev/mapper/${node_3_p[1]}
	umount -dl /dev/mapper/${node_3_p[1]}
	sync
	sleep 1
fi

echo " "
echo "Отключем устройстово ${node_3}"
sync
sleep 3
kpartx -dv ${node_3}
sync
sleep 3
losetup -v -d ${node_3}

echo " "
echo "*******************************************************************************"
echo "Этап 5: Подключем разделы для копирования загрузчика на первый раздел"

echo "Монтируем образ как Loop усройство"

sync
sleep 1
node_6=$(losetup -f --show $TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]")

sync
sleep 1
echo " временное loop устройство: ${node_6}"
node_6_p=( $(kpartx -avs ${node_6} | grep -oE "loop[0-9]p[0-9]") )
echo " подключаем разделы: ${node_6_p[0]}, ${node_6_p[1]}"

echo " Форматируем раздел ядра"
mkdir $TMPFOLDER/img_boot -p; sync
mkfs.vfat /dev/mapper/${node_6_p[0]} -n BOOT-VARMX6 > /dev/zero

sync
sleep 1
echo " "
echo " Копируем ядро и DTS"
mount /dev/mapper/${node_6_p[0]} $TMPFOLDER/img_boot -t vfat > /dev/zero
cp $YOCTO_DEPLOY_LINK/$DTB_NAME $TMPFOLDER/img_boot/imx6q-var-som-res.dtb
cp $YOCTO_DEPLOY_LINK/$DTB_NAME_SOLO $TMPFOLDER/img_boot/imx6dl-var-som-res.dtb
cp $YOCTO_DEPLOY_LINK/$UIMAGE_NAME $TMPFOLDER/img_boot/$UIMAGE_NAME

sync
sleep 1
echo " "
echo " Расширяем раздел root"
mkdir $TMPFOLDER/img_fs -p; sync
mount /dev/mapper/${node_6_p[1]} $TMPFOLDER/img_fs -t ext4
sync
e2label /dev/mapper/${node_6_p[1]} rootfs > /dev/zero
sync

echo " на root fs изначально свободно  $(df --output=avail --block-size=1M /dev/mapper/${node_6_p[1]} | grep -oE "\b[0-9]+") МБайт"

echo " увеличиваем раздел до конца устройства"
sync
sleep 3
resize2fs -F /dev/mapper/${node_6_p[1]}  > /dev/zero
sync

echo " на root fs теперь свободно  $(df --output=avail --block-size=1M /dev/mapper/${node_6_p[1]} | grep -oE "\b[0-9]+") МБайт"
echo " "
echo " Отключем устройстово ${node_6_p[0]}, ${node_6_p[1]}"

#2 раза потому что система маунтит ешё и по fstab и от них не отключает при одном umount
sync
sleep 3
umount -dl /dev/mapper/${node_6_p[1]}
umount -dl /dev/mapper/${node_6_p[1]}

sync
sleep 3
umount -dl /dev/mapper/${node_6_p[0]}
umount -dl /dev/mapper/${node_6_p[0]}

echo " "
echo " Отключем устройстово ${node_6}"
sync
sleep 3
kpartx -dvs ${node_6}
sync
sleep 3
losetup -v -d ${node_6}

echo " "
echo "*******************************************************************************"
echo "Этап 6: Подключем разделы для копирования образов Linux"

echo "Монтируем образ как Loop усройство"

sync
sleep 1
node_6=$(losetup -f --show $TMPFOLDER/$TMPFILE | grep -oE "/dev/loop[0-9]")

sync
sleep 1
echo " временное loop устройство: ${node_6}"
node_6_p=( $(kpartx -avs ${node_6} | grep -oE "loop[0-9]p[0-9]") )
echo " подключаем разделы: ${node_6_p[0]}, ${node_6_p[1]}"

sync
sleep 1
mount /dev/mapper/${node_6_p[0]} $TMPFOLDER/img_boot -t vfat > /dev/zero
mount /dev/mapper/${node_6_p[1]} $TMPFOLDER/img_fs -t ext4
sync
e2label /dev/mapper/${node_6_p[1]} rootfs > /dev/zero

echo " Создаём целевую папку "
mkdir $TMPFOLDER/img_fs/opt/images/Linux -p
chmod -R 777 $TMPFOLDER/img_fs/opt
echo " "
sync

#TODO В оригинале тут у нас заливка скриптов на файловую сиситему
echo " Копируем скрипты авто разворачивания образов "
cp -Rv scripts/recovery_SD_files/rootfs/* $TMPFOLDER/img_fs/
echo " Отключение systemd юнита для AGL, он не нужен в SD версии"
rm $TMPFOLDER/img_fs/etc/systemd/system/local-fs.target.wants/app.service
echo " Меняем права доступа на id_rsa "
chmod 0600 $TMPFOLDER/img_fs/opt/images/ssh-tunnel/.ssh/id_rsa
echo " "
echo " Копируем образы целевой системы "
cp -v $YOCTO_DEPLOY_LINK/$SPL_NAME_TGT $TMPFOLDER/img_fs/opt/images/Linux/
sync
cp -v $YOCTO_DEPLOY_LINK/$UBOOT_NAME_TGT $TMPFOLDER/img_fs/opt/images/Linux/
sync
cp -v $YOCTO_DEPLOY_LINK/$DTB_NAME $TMPFOLDER/img_fs/opt/images/Linux/imx6q-var-som-res.dtb
sync
cp -v $YOCTO_DEPLOY_LINK/$DTB_NAME_SOLO $TMPFOLDER/img_fs/opt/images/Linux/imx6dl-var-som-res.dtb
sync
cp -v $YOCTO_DEPLOY_LINK/$UIMAGE_NAME $TMPFOLDER/img_fs/opt/images/Linux/$UIMAGE_NAME
#dd if=/dev/mapper/${node_6_p[0]} of=$TMPFOLDER/img_fs/opt/images/Linux/$UIMAGE_NAME.iso
sync
cp -v $YOCTO_DEPLOY_LINK/$ROOTFS_NAME $TMPFOLDER/img_fs/opt/images/Linux/
sync
sleep 1
sync

if [ -z "$ISO_EXTRA_INFO_LINK" ]
then
	echo " "
else
	echo " "
	echo "Копируем информацию о собраном софте в образ"
	echo " "
	mkdir  $TMPFOLDER/img_fs/etc/git_os_info
	cp -v $ISO_EXTRA_INFO_LINK/* $TMPFOLDER/img_fs/etc/git_os_info
	echo " "
	sync
	sleep 1
fi

echo " "
echo " на root fs после копирования осталось свободно  $(df --output=avail --block-size=1M /dev/mapper/${node_6_p[1]} | grep -oE "\b[0-9]+") МБайт"
echo " "
echo " Отключем устройстово ${node_6_p[0]}, ${node_6_p[1]}"

#2 раза потому что система маунтит ешё и по fstab и от них не отключает при одном umount
sync
sleep 3
umount -dl /dev/mapper/${node_6_p[1]}
umount -dl /dev/mapper/${node_6_p[1]}

sync
sleep 3
umount -dl /dev/mapper/${node_6_p[0]}
umount -dl /dev/mapper/${node_6_p[0]}

echo " "
echo " Отключем устройстово ${node_6}"
sync
sleep 3
kpartx -dvs ${node_6}
sync
sleep 3
losetup -v -d ${node_6}

rmdir $TMPFOLDER/img_boot
rmdir $TMPFOLDER/img_fs
sync



echo " "
echo "*******************************************************************************"
echo "Готово! Краткая справка:"
echo " Образ SD Карты для установки готов."
echo " Далее этот образ можно залить на SD карту командой:"
echo " sudo dd if=$TMPFOLDER/$TMPFILE of=/dev/... bs=512"
echo " или"
echo " sudo dcfldd if=$TMPFOLDER/$TMPFILE of=/dev/... bs=512"
echo " "
echo " Для проверки образа можно использовать команды:"
echo "  mkdir $TMPFOLDER/img_boot"
echo "  mkdir $TMPFOLDER/img_fs"
echo "  sudo losetup -f --show $TMPFOLDER/$TMPFILE"
echo "  sudo kpartx -av /dev/loop0"
echo "  sudo mount /dev/mapper/loop0p1 $TMPFOLDER/img_boot -t vfat"
echo "  sudo mount /dev/mapper/loop0p2 $TMPFOLDER/img_fs -t ext4"

echo " "
echo " отмаунчиваем, и отключаем образ:"
echo "  sudo umount -dl /dev/mapper/loop0p1"
echo "  sudo umount -dl /dev/mapper/loop0p2"
echo "  sudo kpartx -dv /dev/loop0"
echo "  sudo losetup -dv /dev/loop0"
echo "  rmdir $TMPFOLDER/img_boot"
echo "  rmdir $TMPFOLDER/img_fs"


echo " "
echo "*******************************************************************************"
echo " "
