#!/bin/bash
# Meant to be called by install_android.sh
# Added GPT feature for Android 7.1.2_r9

LOG_STDOUT="/opt/install_stdout.log"
LOG_STDERR="/opt/install_stderr.log"
#LOG_STDOUT="/dev/kmesg"
#LOG_STDERR="/dev/kmesg"

DEPLOY_LINK=/opt/images/Linux
#ISO_DEST_LINK=$2
DEST_DEV=/dev/$1

SPL_NAME_TGT=SPL-nand
UBOOT_NAME_TGT=u-boot.img-nand

DTB_NAME=imx6q-var-som-res.dtb
DTB_NAME_SOLO=imx6dl-var-som-res.dtb
UIMAGE_NAME=uImage
ROOTFS_NAME=ag-image-gui-ag-bdb-mx6.ext4
INSTALL_STEP=0

echo "================================================" >> $LOG_STDERR >> $LOG_STDOUT
echo "============= ЛОГ НАЧИНАЕТСЯ ЗДЕСЬ =============" >> $LOG_STDERR >> $LOG_STDOUT
echo "================================================" >> $LOG_STDERR >> $LOG_STDOUT

function die
{
	echo "ОШИБКА: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации" | tee /dev/kmsg
	exit 1
}

function die_warn
{
	echo "ПРЕДУПРЕЖДЕНИЕ: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации" | tee /dev/kmsg
}

function delete_device
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Удаление текущих разделов"

	dd if=/dev/zero of=${DEST_DEV}   bs=1M count=2      >> $LOG_STDOUT 2>> $LOG_STDERR || true
	sync; sleep 1
}

function create_parts
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Содание новых разделов"

	umount ${DEST_DEV}p*
        sfdisk --force -uS ${DEST_DEV} >> $LOG_STDOUT 2>> $LOG_STDERR << EOF
2186, 2048, 83
4234, 20480, 83
24714, 20480, 83
45194, ,5;
45195, 2048000 ,83
2093196, 2048000, 83
4141197, 20480, 83
4161678, , 83;
EOF
	if [ $? -ne 0 ]; then die; fi

	partx -u ${DEST_DEV} || die
}

function install_bootloader
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Установка загрузчика"

	echo " ... : Очистка NAND"
	flash_erase /dev/mtd0 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	flash_erase /dev/mtd1 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR || die

	echo " ... : Установка NAND загрузчика"

	kobs-ng init -x ${DEPLOY_LINK}/${SPL_NAME_TGT} --search_exponent=1 -v >> $LOG_STDOUT 2>> $LOG_STDERR || die
	nandwrite -p /dev/mtd1 ${DEPLOY_LINK}/${UBOOT_NAME_TGT} >> $LOG_STDOUT 2>> $LOG_STDERR || die
}

function install_linux
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Установка загрузчика и ядра Linux"

	echo " ... : Установка раздела ядра: $bootimage_file"
	umount ${DEST_DEV}p*
	mkdir /mnt/BOOT-VARMX6

	mkfs.ext4 ${DEST_DEV}p2 -FFL kernel1 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	mount ${DEST_DEV}p2 /mnt/BOOT-VARMX6 >> $LOG_STDOUT 2>> $LOG_STDERR || die

	cp "${DEPLOY_LINK}/${DTB_NAME}"      "/mnt/BOOT-VARMX6/$DTB_NAME"      >> $LOG_STDOUT 2>> $LOG_STDERR || die
	cp "${DEPLOY_LINK}/${DTB_NAME_SOLO}" "/mnt/BOOT-VARMX6/$DTB_NAME_SOLO" >> $LOG_STDOUT 2>> $LOG_STDERR || die
	cp "${DEPLOY_LINK}/${UIMAGE_NAME}"   "/mnt/BOOT-VARMX6/$UIMAGE_NAME"   >> $LOG_STDOUT 2>> $LOG_STDERR || die

	sync
	sleep 1
	umount -dl ${DEST_DEV}p2 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	rm -rf /mnt/BOOT-VARMX6
	sync

	echo " ... : Установка образа rootfs: ${DEPLOY_LINK}/${ROOTFS_NAME}"
	dd if=${DEPLOY_LINK}/${ROOTFS_NAME} of=${DEST_DEV}p5 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	#e2label /dev/${DEST_DEV}p3 kernel2
	e2label ${DEST_DEV}p5 root1 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	#e2label /dev/${DEST_DEV}p6 root2

	echo " ... : Установка точек монтирования раздела root1"
	mkdir /mnt/root1
	mount ${DEST_DEV}p5 /mnt/root1
	mkdir /mnt/root1/data
	mkdir /mnt/root1/boot/cfg
	mkdir /mnt/tmp
	mkdir /mnt/root1/etc/git_os_info
	cp -r /etc/git_os_info/* /mnt/root1/etc/git_os_info
	cp -r /mnt/root1/etc/bdb-default-cfg/* /mnt/tmp
	sync
	umount -dl /mnt/root1
	rm -rf /mnt/root1
	sync

	sleep 1
}

function check_fs
{
	label=$1
	dev=$2
	echo " ... : Проверка файловой системы $label на $dev"
	fsck.ext4 -n "$dev" >> $LOG_STDOUT 2>> $LOG_STDERR
	if [ $? -eq 0 ]; then
		echo " ... : $label на $dev существует и исправна, оставляем без изменений"
	else
		echo " ... : $label на $dev ИМЕЕТ СБОЙ с кодом $? ПЕРЕФОРМАТИРУЕМ!"
		mkfs.ext4 -FFL "$label" "$dev" >> $LOG_STDOUT 2>> $LOG_STDERR || die
	fi
}

function setup_other_fs
{
	echo
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Проверка/Раворачивание пользовательских файловых сиситем."

	check_fs "CFG" "${DEST_DEV}p1"

	sync; sleep 1

	check_fs "userid" "${DEST_DEV}p7"

	echo " ... : Установка стандартных файлов конфигураций на userid"
	mkdir /mnt/userid
	mount ${DEST_DEV}p7 /mnt/userid -t ext4 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	cp -vr /mnt/tmp/* /mnt/userid
	rm -rf /mnt/tmp

# Неиспользуется на BDB
#	echo " ... : Копируем ключи для доступа к ssh-tunnel на userid"
#	mkdir -p /mnt/userid/ssh-tunnel/.ssh
#	cp -r /opt/images/ssh-tunnel/.ssh/* /mnt/userid/ssh-tunnel/.ssh

	sync
	umount -dl /mnt/userid >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync
	rm -rf /mnt/userid

	sleep 1
	echo " ... : Создание файловой системы userdt: на ${DEST_DEV}p8"
	mkfs.ext4 -FFL userdt ${DEST_DEV}p8 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync

	echo " "| tee /dev/kmsg
	echo "================================================================" | tee /dev/kmsg
	echo " Внимание работа установщика завершена! Питание можно отключить!" | tee /dev/kmsg
	echo "================================================================" | tee /dev/kmsg
	echo " "| tee /dev/kmsg

	echo " Установлен софт:" | tee /dev/kmsg
	echo "kernel              git hash $rev_kernel" | tee /dev/kmsg
	echo "uboot               git hash $rev_uboot" | tee /dev/kmsg
	echo "meta-autogramma     git hash $meta_autogramma" | tee /dev/kmsg
	echo "agl                 git hash $agl" | tee /dev/kmsg
	echo "================================================================" | tee /dev/kmsg
	echo " "| tee /dev/kmsg
}


install_bootloader
delete_device
create_parts
install_linux
setup_other_fs

