#!/bin/bash
# Meant to be called by install_android.sh

FAILED="1"

# Linux по умолчанию имеет низкие ограничения на запись в dmesg
# Поэтому поднимем лимит
PREV_RATELIMIT=`cat /proc/sys/kernel/printk_ratelimit`
PREV_RATELIMIT_BURST=`cat /proc/sys/kernel/printk_ratelimit_burst`

echo 999999 > /proc/sys/kernel/printk_ratelimit
echo 999999 > /proc/sys/kernel/printk_ratelimit_burst

function term() {
	if [ "$FAILED" == "1" ]; then
		echo "ЧТО-ТО ПОШЛО НЕ ТАК. ПРОЦЕСС ПРОШИВКИ ОСТАНОВЛЕН" | tee /dev/kmsg
	else
		echo "ТЕПЕРЬ ПИТАНИЕ МОЖНО ОТКЛЮЧИТЬ!" | tee /dev/kmsg
	fi
}
trap term 1 2 3 8 14 15

echo "***********************************!!! WARNING !!!***********************************" | tee /dev/kmsg
echo "After 15 seconds, the system will upload images through the launch script /opt/images/autoinstall.sh" | tee /dev/kmsg
echo "After the system performs the script, the power will be turned off." | tee /dev/kmsg
echo "If you want to stop this process, you must login as root and run CMD \"kill $$\"" | tee /dev/kmsg
echo "***********************************!!! WARNING !!!***********************************" | tee /dev/kmsg

sleep 15
echo Start download...
sleep 1
/opt/images/autoinstall.sh mmcblk0 && FAILED="0"
# (sleep 1; shutdown -h now;)

sync

echo $PREV_RATELIMIT  > /proc/sys/kernel/printk_ratelimit
echo $PREV_RATELIMIT_BURST > /proc/sys/kernel/printk_ratelimit_burst

exit 0
